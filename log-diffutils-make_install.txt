Making install in lib
make[1]: Entering directory `/mnt/lfs/sources/diffutils-3.3/lib'
make  install-am
make[2]: Entering directory `/mnt/lfs/sources/diffutils-3.3/lib'
make[3]: Entering directory `/mnt/lfs/sources/diffutils-3.3/lib'
if test yes = no; then \
	  case 'linux-gnu' in \
	    darwin[56]*) \
	      need_charset_alias=true ;; \
	    darwin* | cygwin* | mingw* | pw32* | cegcc*) \
	      need_charset_alias=false ;; \
	    *) \
	      need_charset_alias=true ;; \
	  esac ; \
	else \
	  need_charset_alias=false ; \
	fi ; \
	if $need_charset_alias; then \
	  /bin/sh /mnt/lfs/sources/diffutils-3.3/build-aux/install-sh -d /tools/lib ; \
	fi ; \
	if test -f /tools/lib/charset.alias; then \
	  sed -f ref-add.sed /tools/lib/charset.alias > /tools/lib/charset.tmp ; \
	  /tools/bin/install -c -m 644 /tools/lib/charset.tmp /tools/lib/charset.alias ; \
	  rm -f /tools/lib/charset.tmp ; \
	else \
	  if $need_charset_alias; then \
	    sed -f ref-add.sed charset.alias > /tools/lib/charset.tmp ; \
	    /tools/bin/install -c -m 644 /tools/lib/charset.tmp /tools/lib/charset.alias ; \
	    rm -f /tools/lib/charset.tmp ; \
	  fi ; \
	fi
make[3]: Nothing to be done for `install-data-am'.
make[3]: Leaving directory `/mnt/lfs/sources/diffutils-3.3/lib'
make[2]: Leaving directory `/mnt/lfs/sources/diffutils-3.3/lib'
make[1]: Leaving directory `/mnt/lfs/sources/diffutils-3.3/lib'
Making install in src
make[1]: Entering directory `/mnt/lfs/sources/diffutils-3.3/src'
make  install-am
make[2]: Entering directory `/mnt/lfs/sources/diffutils-3.3/src'
make[3]: Entering directory `/mnt/lfs/sources/diffutils-3.3/src'
 /tools/bin/mkdir -p '/tools/bin'
  /tools/bin/install -c cmp diff diff3 sdiff '/tools/bin'
make[3]: Nothing to be done for `install-data-am'.
make[3]: Leaving directory `/mnt/lfs/sources/diffutils-3.3/src'
make[2]: Leaving directory `/mnt/lfs/sources/diffutils-3.3/src'
make[1]: Leaving directory `/mnt/lfs/sources/diffutils-3.3/src'
Making install in tests
make[1]: Entering directory `/mnt/lfs/sources/diffutils-3.3/tests'
make[2]: Entering directory `/mnt/lfs/sources/diffutils-3.3/tests'
make[2]: Nothing to be done for `install-exec-am'.
make[2]: Nothing to be done for `install-data-am'.
make[2]: Leaving directory `/mnt/lfs/sources/diffutils-3.3/tests'
make[1]: Leaving directory `/mnt/lfs/sources/diffutils-3.3/tests'
Making install in doc
make[1]: Entering directory `/mnt/lfs/sources/diffutils-3.3/doc'
make[2]: Entering directory `/mnt/lfs/sources/diffutils-3.3/doc'
make[2]: Nothing to be done for `install-exec-am'.
 /tools/bin/mkdir -p '/tools/share/info'
 /tools/bin/install -c -m 644 ./diffutils.info '/tools/share/info'
 install-info --info-dir='/tools/share/info' '/tools/share/info/diffutils.info'
make[2]: Leaving directory `/mnt/lfs/sources/diffutils-3.3/doc'
make[1]: Leaving directory `/mnt/lfs/sources/diffutils-3.3/doc'
Making install in man
make[1]: Entering directory `/mnt/lfs/sources/diffutils-3.3/man'
make[2]: Entering directory `/mnt/lfs/sources/diffutils-3.3/man'
make[2]: Nothing to be done for `install-exec-am'.
 /tools/bin/mkdir -p '/tools/share/man/man1'
 /tools/bin/install -c -m 644 cmp.1 diff.1 diff3.1 sdiff.1 '/tools/share/man/man1'
make[2]: Leaving directory `/mnt/lfs/sources/diffutils-3.3/man'
make[1]: Leaving directory `/mnt/lfs/sources/diffutils-3.3/man'
Making install in po
make[1]: Entering directory `/mnt/lfs/sources/diffutils-3.3/po'
installing ca.gmo as /tools/share/locale/ca/LC_MESSAGES/diffutils.mo
installing cs.gmo as /tools/share/locale/cs/LC_MESSAGES/diffutils.mo
installing da.gmo as /tools/share/locale/da/LC_MESSAGES/diffutils.mo
installing de.gmo as /tools/share/locale/de/LC_MESSAGES/diffutils.mo
installing el.gmo as /tools/share/locale/el/LC_MESSAGES/diffutils.mo
installing eo.gmo as /tools/share/locale/eo/LC_MESSAGES/diffutils.mo
installing es.gmo as /tools/share/locale/es/LC_MESSAGES/diffutils.mo
installing fi.gmo as /tools/share/locale/fi/LC_MESSAGES/diffutils.mo
installing fr.gmo as /tools/share/locale/fr/LC_MESSAGES/diffutils.mo
installing ga.gmo as /tools/share/locale/ga/LC_MESSAGES/diffutils.mo
installing gl.gmo as /tools/share/locale/gl/LC_MESSAGES/diffutils.mo
installing he.gmo as /tools/share/locale/he/LC_MESSAGES/diffutils.mo
installing hr.gmo as /tools/share/locale/hr/LC_MESSAGES/diffutils.mo
installing hu.gmo as /tools/share/locale/hu/LC_MESSAGES/diffutils.mo
installing id.gmo as /tools/share/locale/id/LC_MESSAGES/diffutils.mo
installing it.gmo as /tools/share/locale/it/LC_MESSAGES/diffutils.mo
installing ja.gmo as /tools/share/locale/ja/LC_MESSAGES/diffutils.mo
installing lv.gmo as /tools/share/locale/lv/LC_MESSAGES/diffutils.mo
installing ms.gmo as /tools/share/locale/ms/LC_MESSAGES/diffutils.mo
installing nl.gmo as /tools/share/locale/nl/LC_MESSAGES/diffutils.mo
installing pl.gmo as /tools/share/locale/pl/LC_MESSAGES/diffutils.mo
installing pt_BR.gmo as /tools/share/locale/pt_BR/LC_MESSAGES/diffutils.mo
installing ro.gmo as /tools/share/locale/ro/LC_MESSAGES/diffutils.mo
installing ru.gmo as /tools/share/locale/ru/LC_MESSAGES/diffutils.mo
installing sr.gmo as /tools/share/locale/sr/LC_MESSAGES/diffutils.mo
installing sv.gmo as /tools/share/locale/sv/LC_MESSAGES/diffutils.mo
installing tr.gmo as /tools/share/locale/tr/LC_MESSAGES/diffutils.mo
installing uk.gmo as /tools/share/locale/uk/LC_MESSAGES/diffutils.mo
installing vi.gmo as /tools/share/locale/vi/LC_MESSAGES/diffutils.mo
installing zh_CN.gmo as /tools/share/locale/zh_CN/LC_MESSAGES/diffutils.mo
installing zh_TW.gmo as /tools/share/locale/zh_TW/LC_MESSAGES/diffutils.mo
if test "diffutils" = "gettext-tools"; then \
	  @mkdir_p@ /tools/share/gettext/po; \
	  for file in Makefile.in.in remove-potcdate.sin quot.sed boldquot.sed en@quot.header en@boldquot.header insert-header.sin Rules-quot   Makevars.template; do \
	    /tools/bin/install -c -m 644 ./$file \
			    /tools/share/gettext/po/$file; \
	  done; \
	  for file in Makevars; do \
	    rm -f /tools/share/gettext/po/$file; \
	  done; \
	else \
	  : ; \
	fi
make[1]: Leaving directory `/mnt/lfs/sources/diffutils-3.3/po'
Making install in gnulib-tests
make[1]: Entering directory `/mnt/lfs/sources/diffutils-3.3/gnulib-tests'
make  install-recursive
make[2]: Entering directory `/mnt/lfs/sources/diffutils-3.3/gnulib-tests'
Making install in .
make[3]: Entering directory `/mnt/lfs/sources/diffutils-3.3/gnulib-tests'
make[4]: Entering directory `/mnt/lfs/sources/diffutils-3.3/gnulib-tests'
make[4]: Nothing to be done for `install-exec-am'.
make[4]: Nothing to be done for `install-data-am'.
make[4]: Leaving directory `/mnt/lfs/sources/diffutils-3.3/gnulib-tests'
make[3]: Leaving directory `/mnt/lfs/sources/diffutils-3.3/gnulib-tests'
make[2]: Leaving directory `/mnt/lfs/sources/diffutils-3.3/gnulib-tests'
make[1]: Leaving directory `/mnt/lfs/sources/diffutils-3.3/gnulib-tests'
make[1]: Entering directory `/mnt/lfs/sources/diffutils-3.3'
make[2]: Entering directory `/mnt/lfs/sources/diffutils-3.3'
make[2]: Nothing to be done for `install-exec-am'.
make[2]: Nothing to be done for `install-data-am'.
make[2]: Leaving directory `/mnt/lfs/sources/diffutils-3.3'
make[1]: Leaving directory `/mnt/lfs/sources/diffutils-3.3'
