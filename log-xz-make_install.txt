Making install in src
make[1]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src'
Making install in liblzma
make[2]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src/liblzma'
Making install in api
make[3]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src/liblzma/api'
make[4]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src/liblzma/api'
make[4]: Nothing to be done for 'install-exec-am'.
 /tools/bin/mkdir -p '/tools/include'
 /tools/bin/mkdir -p '/tools/include/lzma'
 /tools/bin/install -c -m 644  lzma/base.h lzma/bcj.h lzma/block.h lzma/check.h lzma/container.h lzma/delta.h lzma/filter.h lzma/hardware.h lzma/index.h lzma/index_hash.h lzma/lzma.h lzma/stream_flags.h lzma/version.h lzma/vli.h '/tools/include/lzma'
 /tools/bin/install -c -m 644  lzma.h '/tools/include/.'
make[4]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src/liblzma/api'
make[3]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src/liblzma/api'
make[3]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src/liblzma'
make[4]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src/liblzma'
 /tools/bin/mkdir -p '/tools/lib'
 /bin/bash ../../libtool   --mode=install /tools/bin/install -c   liblzma.la '/tools/lib'
libtool: install: /tools/bin/install -c .libs/liblzma.so.5.0.5 /tools/lib/liblzma.so.5.0.5
libtool: install: (cd /tools/lib && { ln -s -f liblzma.so.5.0.5 liblzma.so.5 || { rm -f liblzma.so.5 && ln -s liblzma.so.5.0.5 liblzma.so.5; }; })
libtool: install: (cd /tools/lib && { ln -s -f liblzma.so.5.0.5 liblzma.so || { rm -f liblzma.so && ln -s liblzma.so.5.0.5 liblzma.so; }; })
libtool: install: /tools/bin/install -c .libs/liblzma.lai /tools/lib/liblzma.la
libtool: install: /tools/bin/install -c .libs/liblzma.a /tools/lib/liblzma.a
libtool: install: chmod 644 /tools/lib/liblzma.a
libtool: install: ranlib /tools/lib/liblzma.a
libtool: finish: PATH="/tools/bin:/bin:/usr/bin:/sbin" ldconfig -n /tools/lib
----------------------------------------------------------------------
Libraries have been installed in:
   /tools/lib

If you ever happen to want to link against installed libraries
in a given directory, LIBDIR, you must either use libtool, and
specify the full pathname of the library, or use the `-LLIBDIR'
flag during linking and do at least one of the following:
   - add LIBDIR to the `LD_LIBRARY_PATH' environment variable
     during execution
   - add LIBDIR to the `LD_RUN_PATH' environment variable
     during linking
   - use the `-Wl,-rpath -Wl,LIBDIR' linker flag
   - have your system administrator add LIBDIR to `/etc/ld.so.conf'

See any operating system documentation about shared libraries for
more information, such as the ld(1) and ld.so(8) manual pages.
----------------------------------------------------------------------
 /tools/bin/mkdir -p '/tools/lib/pkgconfig'
 /tools/bin/install -c -m 644 liblzma.pc '/tools/lib/pkgconfig'
make[4]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src/liblzma'
make[3]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src/liblzma'
make[2]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src/liblzma'
Making install in xzdec
make[2]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src/xzdec'
make[3]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src/xzdec'
 /tools/bin/mkdir -p '/tools/bin'
  /bin/bash ../../libtool   --mode=install /tools/bin/install -c xzdec lzmadec '/tools/bin'
libtool: install: /tools/bin/install -c .libs/xzdec /tools/bin/xzdec
libtool: install: /tools/bin/install -c .libs/lzmadec /tools/bin/lzmadec
 /tools/bin/mkdir -p '/tools/share/man/man1'
 /tools/bin/install -c -m 644 xzdec.1 '/tools/share/man/man1'
make  install-data-hook
make[4]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src/xzdec'
cd /tools/share/man/man1 && \
target=`echo xzdec | sed 's,x,x,'` && \
link=`echo lzmadec | sed 's,x,x,'` && \
rm -f $link.1 && \
ln -s $target.1 $link.1
make[4]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src/xzdec'
make[3]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src/xzdec'
make[2]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src/xzdec'
Making install in xz
make[2]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src/xz'
make[3]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src/xz'
 /tools/bin/mkdir -p '/tools/bin'
  /bin/bash ../../libtool   --mode=install /tools/bin/install -c xz '/tools/bin'
libtool: install: /tools/bin/install -c .libs/xz /tools/bin/xz
make  install-exec-hook
make[4]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src/xz'
cd /tools/bin && \
target=`echo xz | sed 's,x,x,'` && \
for name in unxz xzcat lzma unlzma lzcat; do \
	link=`echo $name | sed 's,x,x,'` && \
	rm -f $link && \
	ln -s $target $link; \
done
make[4]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src/xz'
 /tools/bin/mkdir -p '/tools/share/man/man1'
 /tools/bin/install -c -m 644 xz.1 '/tools/share/man/man1'
make  install-data-hook
make[4]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src/xz'
cd /tools/share/man/man1 && \
target=`echo xz | sed 's,x,x,'` && \
for name in unxz xzcat lzma unlzma lzcat; do \
	link=`echo $name | sed 's,x,x,'` && \
	rm -f $link.1 && \
	ln -s $target.1 $link.1; \
done
make[4]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src/xz'
make[3]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src/xz'
make[2]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src/xz'
Making install in lzmainfo
make[2]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src/lzmainfo'
make[3]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src/lzmainfo'
 /tools/bin/mkdir -p '/tools/bin'
  /bin/bash ../../libtool   --mode=install /tools/bin/install -c lzmainfo '/tools/bin'
libtool: install: /tools/bin/install -c .libs/lzmainfo /tools/bin/lzmainfo
 /tools/bin/mkdir -p '/tools/share/man/man1'
 /tools/bin/install -c -m 644 lzmainfo.1 '/tools/share/man/man1'
make[3]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src/lzmainfo'
make[2]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src/lzmainfo'
Making install in scripts
make[2]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src/scripts'
make[3]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src/scripts'
 /tools/bin/mkdir -p '/tools/bin'
 /tools/bin/install -c xzdiff xzgrep xzmore xzless '/tools/bin'
make  install-exec-hook
make[4]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src/scripts'
cd /tools/bin && \
for pair in xzdiff-xzcmp xzgrep-xzegrep xzgrep-xzfgrep xzdiff-lzdiff xzdiff-lzcmp xzgrep-lzgrep xzgrep-lzegrep xzgrep-lzfgrep xzmore-lzmore xzless-lzless; do \
	target=`echo $pair | sed 's/-.*$//' | sed 's,x,x,'` && \
	link=`echo $pair | sed 's/^.*-//' | sed 's,x,x,'` && \
	rm -f $link && \
	ln -s $target $link; \
done
make[4]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src/scripts'
 /tools/bin/mkdir -p '/tools/share/man/man1'
 /tools/bin/install -c -m 644 xzdiff.1 xzgrep.1 xzmore.1 xzless.1 '/tools/share/man/man1'
make  install-data-hook
make[4]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src/scripts'
cd /tools/share/man/man1 && \
for pair in xzdiff-xzcmp xzgrep-xzegrep xzgrep-xzfgrep xzdiff-lzdiff xzdiff-lzcmp xzgrep-lzgrep xzgrep-lzegrep xzgrep-lzfgrep xzmore-lzmore xzless-lzless; do \
	target=`echo $pair | sed 's/-.*$//' | sed 's,x,x,'` && \
	link=`echo $pair | sed 's/^.*-//' | sed 's,x,x,'` && \
	rm -f $link.1 && \
	ln -s $target.1 $link.1; \
done
make[4]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src/scripts'
make[3]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src/scripts'
make[2]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src/scripts'
make[2]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src'
make[3]: Entering directory '/mnt/lfs/sources/xz-5.0.5/src'
make[3]: Nothing to be done for 'install-exec-am'.
make[3]: Nothing to be done for 'install-data-am'.
make[3]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src'
make[2]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src'
make[1]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/src'
Making install in po
make[1]: Entering directory '/mnt/lfs/sources/xz-5.0.5/po'
installing cs.gmo as /tools/share/locale/cs/LC_MESSAGES/xz.mo
installing de.gmo as /tools/share/locale/de/LC_MESSAGES/xz.mo
installing fr.gmo as /tools/share/locale/fr/LC_MESSAGES/xz.mo
installing it.gmo as /tools/share/locale/it/LC_MESSAGES/xz.mo
installing pl.gmo as /tools/share/locale/pl/LC_MESSAGES/xz.mo
if test "xz" = "gettext-tools"; then \
  /tools/bin/mkdir -p /tools/share/gettext/po; \
  for file in Makefile.in.in remove-potcdate.sin quot.sed boldquot.sed en@quot.header en@boldquot.header insert-header.sin Rules-quot   Makevars.template; do \
    /tools/bin/install -c -m 644 ./$file \
		    /tools/share/gettext/po/$file; \
  done; \
  for file in Makevars; do \
    rm -f /tools/share/gettext/po/$file; \
  done; \
else \
  : ; \
fi
make[1]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/po'
Making install in tests
make[1]: Entering directory '/mnt/lfs/sources/xz-5.0.5/tests'
make[2]: Entering directory '/mnt/lfs/sources/xz-5.0.5/tests'
make[2]: Nothing to be done for 'install-exec-am'.
make[2]: Nothing to be done for 'install-data-am'.
make[2]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/tests'
make[1]: Leaving directory '/mnt/lfs/sources/xz-5.0.5/tests'
make[1]: Entering directory '/mnt/lfs/sources/xz-5.0.5'
make[2]: Entering directory '/mnt/lfs/sources/xz-5.0.5'
make[2]: Nothing to be done for 'install-exec-am'.
 /tools/bin/mkdir -p '/tools/share/doc/xz'
 /tools/bin/install -c -m 644 AUTHORS COPYING COPYING.GPLv2 NEWS README THANKS TODO doc/faq.txt doc/history.txt doc/xz-file-format.txt doc/lzma-file-format.txt '/tools/share/doc/xz'
 /tools/bin/mkdir -p '/tools/share/doc/xz/examples'
 /tools/bin/install -c -m 644 doc/examples/00_README.txt doc/examples/01_compress_easy.c doc/examples/02_decompress.c doc/examples/03_compress_custom.c doc/examples/Makefile '/tools/share/doc/xz/examples'
 /tools/bin/mkdir -p '/tools/share/doc/xz/examples_old'
 /tools/bin/install -c -m 644 doc/examples_old/xz_pipe_comp.c doc/examples_old/xz_pipe_decomp.c '/tools/share/doc/xz/examples_old'
make[2]: Leaving directory '/mnt/lfs/sources/xz-5.0.5'
make[1]: Leaving directory '/mnt/lfs/sources/xz-5.0.5'
